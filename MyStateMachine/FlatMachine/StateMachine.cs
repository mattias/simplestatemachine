﻿using System;
using System.Collections.Generic;

namespace MyStateMachine.FlatMachine
{
	public class StateMachine<TState, TEvent> : IStateMachine<TState, TEvent>
	{
		private readonly Dictionary<TState, InitialState<TState, TEvent>> configuringStateDict;
		private TState currentState;
		private bool started;

		public StateMachine()
		{
			configuringStateDict = new Dictionary<TState, InitialState<TState, TEvent>>();
		}

		public IInitialState<TState, TEvent> In(TState state)
		{
			InitialState<TState, TEvent> configuringState;
			if (!configuringStateDict.TryGetValue(state, out configuringState))
			{
				configuringState = new InitialState<TState, TEvent>(this, state);
				configuringStateDict.Add(state, configuringState);
			}
			return configuringState;
		}

		private InitialState<TState, TEvent> GetConfiguringState(TState state)
		{
			InitialState<TState, TEvent> stateWrapper;
			if (!configuringStateDict.TryGetValue(state, out stateWrapper))
			{
				throw new ArgumentException("UNHANDLED STATE!");
			}
			return stateWrapper;
		}

		public void Handle(TEvent stateEvent)
		{
			if (!started)
				throw new ArgumentException("NOT STARTED YET!");

			GetConfiguringState(currentState).Handle(stateEvent);
		}

		public void Start(TState initialState)
		{
			if (started)
				throw new ArgumentException("ALREADY STARTED!");
			started = true;
			CurrentState = initialState;
		}

		public TState CurrentState
		{
			get { return currentState; }
			internal set
			{
				InitialState<TState, TEvent> exitingState;
				if (configuringStateDict.TryGetValue(currentState, out exitingState))
				{
					exitingState.Exiting();
				}

				currentState = value;

				InitialState<TState, TEvent> enteringState;
				if (configuringStateDict.TryGetValue(currentState, out enteringState))
				{
					enteringState.Entering();
				}
			}
		}
	}
}