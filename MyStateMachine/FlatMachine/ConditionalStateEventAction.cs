﻿using System;

namespace MyStateMachine.FlatMachine
{
	internal class ConditionalStateEventAction<TState, TEvent> : IConditionalStateEventAction<TState, TEvent>
	{
		private readonly InitialState<TState, TEvent> fromState;
		private readonly TEvent stateEvent;
		private readonly Func<bool> condition;

		internal ConditionalStateEventAction(StateEventAction<TState, TEvent> stateEventAction,
			InitialState<TState, TEvent> fromState, TEvent stateEvent, Func<bool> condition)
		{
			StateEventAction = stateEventAction;
			this.fromState = fromState;
			this.stateEvent = stateEvent;
			this.condition = condition;
		}

		internal StateEventAction<TState, TEvent> StateEventAction { get; private set; }

		public IConditionalState<TState, TEvent> GoTo(TState state)
		{
			fromState.ConfigureEvent(stateEvent, condition, state);
			return new ConditionalState<TState, TEvent>(StateEventAction, fromState, stateEvent);
		}

		public IConditionalState<TState, TEvent> Execute(Action action)
		{
			fromState.ConfigureEvent(stateEvent, condition, action);
			return new ConditionalState<TState, TEvent>(StateEventAction, fromState, stateEvent);
		}
	}
}