﻿using System;
using System.Collections.Generic;

namespace MyStateMachine.FlatMachine
{
	internal class InitialState<TState, TEvent> : IInitialState<TState, TEvent>
	{
		private readonly StateMachine<TState, TEvent> stateMachine;
		private readonly List<Action> enterActions;
		private readonly List<Action> exitActions;
		private readonly Dictionary<TEvent, StateAction> eventActionDict = new Dictionary<TEvent, StateAction>();

		public InitialState(StateMachine<TState, TEvent> stateMachine, TState state)
		{
			this.stateMachine = stateMachine;
			State = state;
			enterActions = new List<Action>();
			exitActions = new List<Action>();
		}

		internal TState State { get; private set; }

		public IInitialState<TState, TEvent> ExecuteOnEnter(Action action)
		{
			enterActions.Add(action);
			return this;
		}

		public IInitialState<TState, TEvent> ExecuteOnExit(Action action)
		{
			exitActions.Add(action);
			return this;
		}

		public IStateEventAction<TState, TEvent> On(TEvent stateEvent)
		{
			return new StateEventAction<TState, TEvent>(this, stateEvent);
		}

		internal void Entering()
		{
			foreach (var enterAction in enterActions)
			{
				enterAction();
			}
		}

		internal void Exiting()
		{
			foreach (var exitAction in exitActions)
			{
				exitAction();
			}
		}

		public void ConfigureEvent(TEvent stateEvent, TState gotoState)
		{
			Action action = () => { stateMachine.CurrentState = gotoState; };
			ConfigureEvent(stateEvent, action);
		}

		public void ConfigureEvent(TEvent stateEvent, Action action)
		{
			ConfigureEvent(stateEvent, new StateAction(action));
		}

		public void ConfigureEvent(TEvent stateEvent, Func<bool> condition, TState gotoState)
		{
			Action action = () => { stateMachine.CurrentState = gotoState; };
			ConfigureEvent(stateEvent, condition, action);
		}

		public void ConfigureEvent(TEvent stateEvent, Func<bool> condition, Action action)
		{
			ConfigureEvent(stateEvent, new StateAction(condition, action));
		}

		private void ConfigureEvent(TEvent stateEvent, StateAction stateAction)
		{
			StateAction currentAction;
			if (eventActionDict.TryGetValue(stateEvent, out currentAction))
			{
				currentAction.SetNextAction(stateAction);
			}
			else
			{
				eventActionDict.Add(stateEvent, stateAction);
			}
		}

		public void Handle(TEvent stateEvent)
		{
			StateAction stateAction;
			if (!eventActionDict.TryGetValue(stateEvent, out stateAction))
			{
				throw new ArgumentException("UNHANDLED EVENT!");
			}
			stateAction.Execute();
		}
	}
}