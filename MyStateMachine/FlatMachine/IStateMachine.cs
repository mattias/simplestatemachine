﻿using System;

namespace MyStateMachine.FlatMachine
{
	public interface IStateMachine<TState, TEvent>
	{
		IInitialState<TState, TEvent> In(TState state);

		void Handle(TEvent stateEvent);

		void Start(TState initialState);

		TState CurrentState { get; }
	}

	public interface IInitialState<TState, TEvent> : IState<TState, TEvent>
	{
		IInitialState<TState, TEvent> ExecuteOnEnter(Action action);

		IInitialState<TState, TEvent> ExecuteOnExit(Action action);
	}

	public interface IState<TState, TEvent>
	{
		IStateEventAction<TState, TEvent> On(TEvent stateEvent);
	}

	public interface IStateEventAction<TState, TEvent>
	{
		IState<TState, TEvent> GoTo(TState state);

		IState<TState, TEvent> Execute(Action action);

		IConditionalStateEventAction<TState, TEvent> If(Func<bool> condition);
	}

	public interface IConditionalStateEventAction<TState, TEvent>
	{
		IConditionalState<TState, TEvent> GoTo(TState state);

		IConditionalState<TState, TEvent> Execute(Action action);
	}

	public interface IConditionalState<TState, TEvent>
	{
		IConditionalStateEventAction<TState, TEvent> ElseIf(Func<bool> condition);

		IStateEventAction<TState, TEvent> Else();

		IStateEventAction<TState, TEvent> On(TEvent stateEvent); // Ending the IF
	}
}