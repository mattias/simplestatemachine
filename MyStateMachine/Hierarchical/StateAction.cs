﻿using System;

namespace MyStateMachine.Hierarchical
{
	internal class StateAction
	{
		private readonly Func<bool> condition;
		private readonly Action action;
		private StateAction nextAction;

		public StateAction(Action action)
		{
			this.action = action;
		}

		public StateAction(Func<bool> condition, Action action)
		{
			this.condition = condition;
			this.action = action;
		}

		public void SetNextAction(StateAction theNextAction)
		{
			if (condition == null)
				throw new ArgumentException("EVENT ALREADY HANDLED UNCONDITIONALLY");

			if (nextAction == null)
				nextAction = theNextAction;
			else
				nextAction.SetNextAction(theNextAction);
		}

		public void Execute()
		{
			if ((condition == null) || condition())
				action();
			else if (nextAction != null)
				nextAction.Execute();
		}
	}
}