﻿using System;

namespace MyStateMachine.Hierarchical
{
	internal class StateEventAction<TState, TEvent> : IStateEventAction<TState, TEvent>
	{
		private readonly InitialState<TState, TEvent> fromState;
		private readonly TEvent stateEvent;

		internal StateEventAction(InitialState<TState, TEvent> fromState, TEvent stateEvent)
		{
			this.fromState = fromState;
			this.stateEvent = stateEvent;
		}

		public IState<TState, TEvent> GoTo(TState state)
		{
			fromState.ConfigureEvent(stateEvent, state);
			return fromState;
		}

		public IState<TState, TEvent> Execute(Action action)
		{
			fromState.ConfigureEvent(stateEvent, action);
			return fromState;
		}

		public IConditionalStateEventAction<TState, TEvent> If(Func<bool> condition)
		{
			return new ConditionalStateEventAction<TState, TEvent>(this, fromState, stateEvent, condition);
		}
	}
}