﻿using System;

namespace MyStateMachine.Hierarchical
{
	internal class ConditionalState<TState, TEvent> : IConditionalState<TState, TEvent>
	{
		private readonly InitialState<TState, TEvent> fromState;
		private readonly TEvent stateEvent;

		internal ConditionalState(StateEventAction<TState, TEvent> stateEventAction,
			InitialState<TState, TEvent> fromState, TEvent stateEvent)
		{
			StateEventAction = stateEventAction;
			this.fromState = fromState;
			this.stateEvent = stateEvent;
		}

		internal StateEventAction<TState, TEvent> StateEventAction { get; private set; }

		public IConditionalStateEventAction<TState, TEvent> ElseIf(Func<bool> condition)
		{
			return new ConditionalStateEventAction<TState, TEvent>(StateEventAction, fromState, stateEvent, condition);
		}

		public IStateEventAction<TState, TEvent> Else()
		{
			return StateEventAction;
		}

		public IStateEventAction<TState, TEvent> On(TEvent stateEvent)
		{
			return fromState.On(stateEvent);
		}
	}
}