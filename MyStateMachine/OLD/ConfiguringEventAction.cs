﻿using System;

namespace MyStateMachine.OLD
{
	internal class ConfiguringEventAction<TState, TEvent>
	{
		private readonly ConfiguringState<TState, TEvent> fromTransition;
		private Func<bool> conditionFunc;
		private Action unconditionalAction;
		private IfConfiguringEventAction<TState, TEvent> conditionAction;

		public ConfiguringEventAction(ConfiguringState<TState, TEvent> fromTransition)
		{
			this.fromTransition = fromTransition;
		}

		public ConfiguringState<TState, TEvent> GoTo(TState state)
		{
			unconditionalAction = () =>
			{
				fromTransition.StateMachine.CurrentState = state;
			};
			return fromTransition;
		}

		public ConfiguringState<TState, TEvent> Execute(Action action)
		{
			return fromTransition;
		}

		internal void Handle()
		{
			if (unconditionalAction != null)
				unconditionalAction();
			else if (conditionFunc())
				conditionAction.Handle();
		}

		public IfConfiguringEventAction<TState, TEvent> If(Func<bool> condition)
		{
			throw new NotImplementedException();
			//if (elseAction != null)
			//{
			//    throw new ArgumentException("IF-CONDITION AFTER ELSE NOT ALLOWED!");
			//}

			//conditionFunc = condition;
			//conditionAction = new IfConfiguringEventAction<TState, TEvent>(fromTransition);
			//return conditionAction;
		}

		public ConfiguringEventAction<TState, TEvent> Else()
		{
			throw new NotImplementedException();
			//if (conditionFunc == null)
			//{
			//    throw new ArgumentException("ELSE-CONDITION WITHOUT IF NOT ALLOWED!");
			//}

			//conditionAction = new ConfiguringEventAction<TState, TEvent>(fromTransition);
			//return conditionAction;
		}
	}
}