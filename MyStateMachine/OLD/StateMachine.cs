﻿using System;
using System.Collections.Generic;

namespace MyStateMachine.OLD
{
	internal class StateMachine<TState, TEvent>
	{
		private readonly Dictionary<TState, ConfiguringState<TState, TEvent>> configuringStateDict;
		private TState initialState;
		private TState currentState;
		private bool started;

		public StateMachine(TState initialState)
		{
			configuringStateDict = new Dictionary<TState, ConfiguringState<TState, TEvent>>();
			this.initialState = initialState;
		}

		public ConfiguringState<TState, TEvent> In(TState state)
		{
			ConfiguringState<TState, TEvent> configuringState;
			if (!configuringStateDict.TryGetValue(state, out configuringState))
			{
				configuringState = new ConfiguringState<TState, TEvent>(this, state);
				configuringStateDict.Add(state, configuringState);
			}
			return configuringState;
		}

		private ConfiguringState<TState, TEvent> GetConfiguringState(TState state)
		{
			ConfiguringState<TState, TEvent> configuringState;
			if (!configuringStateDict.TryGetValue(state, out configuringState))
			{
				throw new ArgumentException("UNHANDLED STATE!");
			}
			return configuringState;
		}

		public void Handle(TEvent e)
		{
			GetConfiguringState(currentState).Handle(e);
		}

		public TState CurrentState
		{
			get { return currentState; }
			internal set
			{
				ConfiguringState<TState, TEvent> exitingState;
				if (configuringStateDict.TryGetValue(currentState, out exitingState))
				{
					exitingState.OnExit();
				}

				currentState = value;

				ConfiguringState<TState, TEvent> enteringState;
				if (configuringStateDict.TryGetValue(currentState, out enteringState))
				{
					enteringState.OnEnter();
				}
			}
		}

		public void Start()
		{
			if (started)
				throw new ArgumentException("ALREADY STARTED!");
			started = true;
			CurrentState = initialState;
		}
	}
}