﻿using System;

namespace MyStateMachine.OLD
{
	internal class IfConfiguringEventAction<TState, TEvent>
	{
		private readonly ConfiguringState<TState, TEvent> fromTransition;
		private Func<bool> conditionFunc;
		private Action unconditionalAction;
		private ConfiguringEventAction<TState, TEvent> conditionAction;
		private ConfiguringEventAction<TState, TEvent> elseAction;

		public IfConfiguringEventAction(ConfiguringState<TState, TEvent> fromTransition)
		{
			this.fromTransition = fromTransition;
		}

		public IfConfiguringState<TState, TEvent> GoTo(TState state)
		{
			throw new NotImplementedException();

			//unconditionalAction = () =>
			//{
			//    fromTransition.StateMachine.CurrentState = state;
			//};
			//return fromTransition;
		}

		public IfConfiguringState<TState, TEvent> Execute(Action action)
		{
			throw new NotImplementedException();
			//return fromTransition;
		}

		internal void Handle()
		{
			if (unconditionalAction != null)
				unconditionalAction();
			else if (conditionFunc())
				conditionAction.Handle();
			else if (elseAction != null)
				elseAction.Handle();
		}
	}
}