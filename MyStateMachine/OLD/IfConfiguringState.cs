﻿using System;
using System.Collections.Generic;

namespace MyStateMachine.OLD
{
	internal class IfConfiguringState<TState, TEvent>
	{
		private readonly Dictionary<TEvent, ConfiguringEventAction<TState, TEvent>> eventActionDict;
		private readonly List<Action> enterActions;
		private readonly List<Action> exitActions;
		private readonly List<Action> actions;

		internal IfConfiguringState(StateMachine<TState, TEvent> stateMachine, TState state)
		{
			StateMachine = stateMachine;
			State = state;
			eventActionDict = new Dictionary<TEvent, ConfiguringEventAction<TState, TEvent>>();
			enterActions = new List<Action>();
			exitActions = new List<Action>();
			actions = new List<Action>();
		}

		internal StateMachine<TState, TEvent> StateMachine { get; private set; }

		internal TState State { get; private set; }

		public IfConfiguringEventAction<TState, TEvent> ElseIf(Func<bool> condition)
		{
			throw new NotImplementedException();
		}

		public IfConfiguringEventAction<TState, TEvent> Else()
		{
			throw new NotImplementedException();
		}

		public ConfiguringState<TState, TEvent> ExecuteOnEnter(Action action)
		{
			throw new NotImplementedException();

			//enterActions.Add(action);
			//return this;
		}

		public ConfiguringState<TState, TEvent> ExecuteOnExit(Action action)
		{
			throw new NotImplementedException();
			//exitActions.Add(action);
			//return this;
		}

		public ConfiguringState<TState, TEvent> Execute(Action action)
		{
			throw new NotImplementedException();
			//actions.Add(action);
			//return this;
		}

		public ConfiguringEventAction<TState, TEvent> On(TEvent stateEvent)
		{
			throw new NotImplementedException();

			//ConfiguringEventAction<TState, TEvent> eventAction;
			//if (eventActionDict.TryGetValue(stateEvent, out eventAction))
			//{
			//    throw new ArgumentException("EVENT ALREADY CONFIGURED!");
			//}
			//eventAction = new ConfiguringEventAction<TState, TEvent>(this);
			//eventActionDict.Add(stateEvent, eventAction);
			//return eventAction;
		}

		internal void Handle(TEvent e)
		{
			ConfiguringEventAction<TState, TEvent> eventAction;
			if (!eventActionDict.TryGetValue(e, out eventAction))
			{
				throw new ArgumentException("UNHANDLED EVENT!");
			}
			eventAction.Handle();
		}

		internal void OnEnter()
		{
			foreach (var enterAction in enterActions)
			{
				enterAction();
			}
		}

		internal void OnExit()
		{
			foreach (var exitAction in exitActions)
			{
				exitAction();
			}
		}
	}
}