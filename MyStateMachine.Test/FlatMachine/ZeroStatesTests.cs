﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MyStateMachine.FlatMachine
{
	[TestClass]
	public class ZeroStatesTests
	{
		private Hierarchical.StateMachine<S, E> sm;

		[TestInitialize]
		public void TestInitialize()
		{
			sm = new Hierarchical.StateMachine<S, E>();
		}

		[TestMethod]
		public void Create_sm_not_null()
		{
			Assert.IsNotNull(sm);
		}

		[TestMethod]
		public void Current_state_is_default_from_beginning()
		{
			Assert.AreEqual(S.None, sm.CurrentState);
		}

		[TestMethod]
		public void Can_start_empty_sm()
		{
			sm.Start(S.None);
		}

		[TestMethod, ExpectedException(typeof(ArgumentException))]
		public void Start_twice_causes_exception()
		{
			sm.Start(S.None);
			sm.Start(S.None);
		}

		[TestMethod, ExpectedException(typeof(ArgumentException))]
		public void Injecting_events_when_not_started_causes_exception()
		{
			sm.Handle(E.None);
		}

		[TestMethod, ExpectedException(typeof(ArgumentException))]
		public void Injecting_unhandled_event_causes_exception()
		{
			sm.Start(S.None);
			sm.Handle(E.None);
		}
	}
}