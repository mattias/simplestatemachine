﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MyStateMachine.FlatMachine
{
	[TestClass]
	public class ConditionalEventsTests
	{
		private List<string> actions;

		[TestInitialize]
		public void TestInitialize()
		{
			actions = new List<string>();
		}

		[TestMethod]
		public void False_condition_and_no_Else_then_no_GoTo_state()
		{
			var flag = false;
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1).If(() => flag).GoTo(S.B);
			sm.Start(S.A);

			sm.Handle(E.Ev1);

			Assert.AreEqual(S.A, sm.CurrentState);
		}

		[TestMethod]
		public void True_condition_and_no_Else_then_GoTo_state()
		{
			var flag = false;
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1).If(() => flag).GoTo(S.B);
			sm.Start(S.A);

			flag = true;
			sm.Handle(E.Ev1);

			Assert.AreEqual(S.B, sm.CurrentState);
		}

		[TestMethod]
		public void False_condition_and_no_Else_then_no_Executes_action()
		{
			var flag = false;
			var actionExecuted = false;
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1).If(() => flag).Execute(() => actionExecuted = true);
			sm.Start(S.A);

			sm.Handle(E.Ev1);

			Assert.IsFalse(actionExecuted);
		}

		[TestMethod]
		public void True_condition_and_no_Else_then_Executes_action()
		{
			var flag = false;
			var actionExecuted = false;
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1).If(() => flag).Execute(() => actionExecuted = true);
			sm.Start(S.A);

			flag = true;
			sm.Handle(E.Ev1);

			Assert.IsTrue(actionExecuted);
		}

		[TestMethod]
		public void False_condition_with_Else_then_GoTo_Else_state()
		{
			var flag = false;
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1).If(() => flag).GoTo(S.B).Else().GoTo(S.C);
			sm.Start(S.A);

			sm.Handle(E.Ev1);

			Assert.AreEqual(S.C, sm.CurrentState);
		}

		[TestMethod]
		public void True_condition_with_Else_then_GoTo_state()
		{
			var flag = false;
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1).If(() => flag).GoTo(S.B).Else().GoTo(S.C);
			sm.Start(S.A);

			flag = true;
			sm.Handle(E.Ev1);

			Assert.AreEqual(S.B, sm.CurrentState);
		}

		[TestMethod]
		public void False_condition_and_no_Else_then_no_GoTo_state__WITH_two_events_configured()
		{
			var flag = false;
			var sm = new StateMachine<S, E>();
			sm.In(S.A)
				.On(E.Ev1).If(() => flag).GoTo(S.B)
				.On(E.Ev2).Execute(() => { });
			sm.Start(S.A);

			sm.Handle(E.Ev1);

			Assert.AreEqual(S.A, sm.CurrentState);
		}

		[TestMethod]
		public void True_condition_and_no_Else_then_GoTo_state__WITH_two_events_configured()
		{
			var flag = false;
			var sm = new StateMachine<S, E>();
			sm.In(S.A)
				.On(E.Ev1).If(() => flag).GoTo(S.B)
				.On(E.Ev2).Execute(() => { });
			sm.Start(S.A);

			flag = true;
			sm.Handle(E.Ev1);

			Assert.AreEqual(S.B, sm.CurrentState);
		}

		[TestMethod]
		public void If_ElseIf_and_no_Else_and_first_if_condition_then_Execute_action()
		{
			var value = 0;
			var action = "";
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1)
				.If(() => value == 1).Execute(() => action = "1")
				.ElseIf(() => value == 2).Execute(() => action = "2");
			sm.Start(S.A);

			value = 1;
			sm.Handle(E.Ev1);

			Assert.AreEqual(S.A, sm.CurrentState);
			Assert.AreEqual("1", action);
		}

		[TestMethod]
		public void If_ElseIf_and_no_Else_and_elseif_condition_then_Execute_action()
		{
			var value = 0;
			var action = "";
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1)
				.If(() => value == 1).Execute(() => action = "1")
				.ElseIf(() => value == 2).Execute(() => action = "2");
			sm.Start(S.A);

			value = 2;
			sm.Handle(E.Ev1);

			Assert.AreEqual(S.A, sm.CurrentState);
			Assert.AreEqual("2", action);
		}

		[TestMethod]
		public void If_ElseIf_and_no_Else_and_no_match_then_do_nothing()
		{
			var value = 0;
			var action = "";
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1)
				.If(() => value == 1).Execute(() => action = "1")
				.ElseIf(() => value == 2).Execute(() => action = "2");
			sm.Start(S.A);

			value = 3;
			sm.Handle(E.Ev1);

			Assert.AreEqual(S.A, sm.CurrentState);
			Assert.AreEqual("", action);
		}

		[TestMethod]
		public void If_ElseIf_ElseIf_and_no_Else_and_first_if_condition_then_Execute_action()
		{
			var value = 0;
			var action = "";
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1)
				.If(() => value == 1).Execute(() => action = "1")
				.ElseIf(() => value == 2).Execute(() => action = "2")
				.ElseIf(() => value == 3).Execute(() => action = "3");
			sm.Start(S.A);

			value = 1;
			sm.Handle(E.Ev1);

			Assert.AreEqual(S.A, sm.CurrentState);
			Assert.AreEqual("1", action);
		}

		[TestMethod]
		public void If_ElseIf_ElseIf_and_no_Else_and_first_elseif_condition_then_Execute_action()
		{
			var value = 0;
			var action = "";
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1)
				.If(() => value == 1).Execute(() => action = "1")
				.ElseIf(() => value == 2).Execute(() => action = "2")
				.ElseIf(() => value == 3).Execute(() => action = "3");
			sm.Start(S.A);

			value = 2;
			sm.Handle(E.Ev1);

			Assert.AreEqual(S.A, sm.CurrentState);
			Assert.AreEqual("2", action);
		}

		[TestMethod]
		public void If_ElseIf_ElseIf_and_no_Else_and_second_elseif_condition_then_Execute_action()
		{
			var value = 0;
			var action = "";
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1)
				.If(() => value == 1).Execute(() => action = "1")
				.ElseIf(() => value == 2).Execute(() => action = "2")
				.ElseIf(() => value == 3).Execute(() => action = "3");
			sm.Start(S.A);

			value = 3;
			sm.Handle(E.Ev1);

			Assert.AreEqual(S.A, sm.CurrentState);
			Assert.AreEqual("3", action);
		}

		[TestMethod]
		public void If_ElseIf_ElseIf_and_no_Else_and_no_match_then_do_nothing()
		{
			var value = 0;
			var action = "";
			var sm = new StateMachine<S, E>();
			sm.In(S.A).On(E.Ev1)
				.If(() => value == 1).Execute(() => action = "1")
				.ElseIf(() => value == 2).Execute(() => action = "2")
				.ElseIf(() => value == 3).Execute(() => action = "3");
			sm.Start(S.A);

			value = 4;
			sm.Handle(E.Ev1);

			Assert.AreEqual(S.A, sm.CurrentState);
			Assert.AreEqual("", action);
		}
	}
}