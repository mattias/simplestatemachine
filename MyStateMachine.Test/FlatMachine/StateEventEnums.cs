﻿namespace MyStateMachine.FlatMachine
{
	public enum S
	{
		None,
		A,
		B,
		C,
		D
	}

	public enum E
	{
		None,
		Ev1,
		Ev2,
		Ev3,
		Ev4
	}
}