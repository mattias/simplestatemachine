﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MyStateMachine.FlatMachine
{
	[TestClass]
	public class OneStateTests
	{
		private Hierarchical.StateMachine<S, E> sm;
		private List<string> actions;

		[TestInitialize]
		public void TestInitialize()
		{
			sm = new Hierarchical.StateMachine<S, E>();
			actions = new List<string>();
		}

		[TestMethod, ExpectedException(typeof(ArgumentException))]
		public void Configure_same_event_twice_fails()
		{
			sm.In(S.A)
				.On(E.Ev1).GoTo(S.A)
				.On(E.Ev1).GoTo(S.B);
		}

		[TestMethod]
		public void Entering_initial_state_once()
		{
			var entered = 0;
			sm.In(S.A)
				.ExecuteOnEnter(() => entered++);

			sm.Start(S.A);

			Assert.AreEqual(1, entered);
			Assert.AreEqual(S.A, sm.CurrentState);
		}

		[TestMethod]
		public void Many_enter_actions_are_executed_in_order()
		{
			sm.In(S.A)
				.ExecuteOnEnter(() => actions.Add("Enter1"))
				.ExecuteOnEnter(() => actions.Add("Enter2"))
				.ExecuteOnEnter(() => actions.Add("Enter3"))
				.ExecuteOnEnter(() => actions.Add("Enter4"));

			sm.Start(S.A);

			CollectionAssert.AreEqual(new[] { "Enter1", "Enter2", "Enter3", "Enter4" }, actions);
		}

		[TestMethod]
		public void Reentering_initial_state()
		{
			sm.In(S.A)
				.ExecuteOnEnter(() => actions.Add("Enter"))
				.ExecuteOnExit(() => actions.Add("Exit"))
				.On(E.Ev1).GoTo(S.A);

			sm.Start(S.A);
			sm.Handle(E.Ev1);

			CollectionAssert.AreEqual(new[] { "Enter", "Exit", "Enter" }, actions);
		}

		[TestMethod]
		public void Reentering_initial_state_with_many_enter_and_exit_actions_are_executed_in_order()
		{
			sm.In(S.A)
				.ExecuteOnEnter(() => actions.Add("Enter1"))
				.ExecuteOnEnter(() => actions.Add("Enter2"))
				.ExecuteOnEnter(() => actions.Add("Enter3"))
				.ExecuteOnExit(() => actions.Add("Exit1"))
				.ExecuteOnExit(() => actions.Add("Exit2"))
				.ExecuteOnExit(() => actions.Add("Exit3"))
				.On(E.Ev1).GoTo(S.A);

			sm.Start(S.A);
			sm.Handle(E.Ev1);

			CollectionAssert.AreEqual(new[]
			{
			    "Enter1", "Enter2", "Enter3",
			    "Exit1", "Exit2", "Exit3",
			    "Enter1", "Enter2", "Enter3",
			}, actions);
		}

		[TestMethod, ExpectedException(typeof(ArgumentException))]
		public void Injecting_unhandled_event_fails()
		{
			sm.In(S.A)
				.On(E.Ev1).GoTo(S.A);

			sm.Start(S.A);
			sm.Handle(E.Ev2);
		}
	}
}