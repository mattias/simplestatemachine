﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MyStateMachine.FlatMachine
{
	[TestClass]
	public class TwoStatesTests
	{
		private Hierarchical.StateMachine<S, E> sm;
		private List<string> actions;

		[TestInitialize]
		public void TestInitialize()
		{
			sm = new Hierarchical.StateMachine<S, E>();
			actions = new List<string>();

			// Ev1 -> Go to the other state
			// Ev2 -> Go to the same state
			// Ev3 -> Execute an action (but stay in state)

			sm.In(S.A)
				.ExecuteOnEnter(() => actions.Add(">A"))
				.ExecuteOnExit(() => actions.Add("A>"))
				.On(E.Ev1).GoTo(S.B)
				.On(E.Ev2).GoTo(S.A)
				.On(E.Ev3).Execute(() => actions.Add("A!"));

			sm.In(S.B)
				.ExecuteOnEnter(() => actions.Add(">B"))
				.ExecuteOnExit(() => actions.Add("B>"))
				.On(E.Ev1).GoTo(S.A)
				.On(E.Ev2).GoTo(S.B)
				.On(E.Ev3).Execute(() => actions.Add("B!"));
		}

		private void AssertActions(string expectedActions, S startState, S endState, params E[] events)
		{
			sm.Start(startState);
			foreach (var e in events)
				sm.Handle(e);

			var actualActions = string.Join(",", actions);
			Assert.AreEqual(expectedActions, actualActions);
			Assert.AreEqual(endState, sm.CurrentState);
		}

		[TestMethod]
		public void Enter_A_when_starting()
		{
			AssertActions(">A", S.A, S.A);
		}

		[TestMethod]
		public void Handle_event_E1()
		{
			AssertActions(">A,A>,>B", S.A, S.B, E.Ev1);
		}

		[TestMethod]
		public void Handle_event_E2()
		{
			AssertActions(">A,A>,>A", S.A, S.A, E.Ev2);
		}

		[TestMethod]
		public void Handle_event_E3()
		{
			AssertActions(">A,A!", S.A, S.A, E.Ev3);
		}

		[TestMethod]
		public void Handle_event_E1_E1()
		{
			AssertActions(">A,A>,>B,B>,>A", S.A, S.A, E.Ev1, E.Ev1);
		}

		[TestMethod]
		public void Handle_event_E1_E2()
		{
			AssertActions(">A,A>,>B,B>,>B", S.A, S.B, E.Ev1, E.Ev2);
		}

		[TestMethod]
		public void Handle_event_E1_E3()
		{
			AssertActions(">A,A>,>B,B!", S.A, S.B, E.Ev1, E.Ev3);
		}

		[TestMethod]
		public void Handle_event_E2_E1()
		{
			AssertActions(">A,A>,>A,A>,>B", S.A, S.B, E.Ev2, E.Ev1);
		}

		[TestMethod]
		public void Handle_event_E2_E2()
		{
			AssertActions(">A,A>,>A,A>,>A", S.A, S.A, E.Ev2, E.Ev2);
		}

		[TestMethod]
		public void Handle_event_E2_E3()
		{
			AssertActions(">A,A>,>A,A!", S.A, S.A, E.Ev2, E.Ev3);
		}

		[TestMethod]
		public void Handle_event_E3_E1()
		{
			AssertActions(">A,A!,A>,>B", S.A, S.B, E.Ev3, E.Ev1);
		}

		[TestMethod]
		public void Handle_event_E3_E2()
		{
			AssertActions(">A,A!,A>,>A", S.A, S.A, E.Ev3, E.Ev2);
		}

		[TestMethod]
		public void Handle_event_E3_E3()
		{
			AssertActions(">A,A!,A!", S.A, S.A, E.Ev3, E.Ev3);
		}
	}
}