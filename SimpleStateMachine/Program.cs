﻿using System;
using MyStateMachine;
using MyStateMachine.Hierarchical;

namespace SimpleStateMachine
{
	internal class Program
	{
		public enum State
		{
			None,
			A,
			B,
			C,
			D
		}

		public enum SEvent
		{
			None,
			E1,
			E2,
			E3,
			E4
		}

		private static void Main(string[] args)
		{
			var flag = true;
			var value = 0;

			var sm = GetStateMachine<State, SEvent>();

			sm.In(State.A)
				.ExecuteOnEnter(() => WriteEnter("A1"))
				.ExecuteOnExit(() => WriteExit("A1"))
				.On(SEvent.E1).GoTo(State.B)
				.On(SEvent.E2).Execute(() => WriteExecute("E3 happened"))
				.On(SEvent.E3).GoTo(State.C);

			sm.In(State.B)
				.ExecuteOnEnter(() => WriteEnter("B"))
				.ExecuteOnExit(() => WriteExit("B"))
				.On(SEvent.E1)
					.If(() => flag).GoTo(State.C)
					.Else().GoTo(State.D)
				.On(SEvent.E2)
					.If(() => value == 0).GoTo(State.A)
					.ElseIf(() => value == 1).GoTo(State.B)
					.ElseIf(() => value == 2).GoTo(State.C)
					.Else().GoTo(State.D)
				.On(SEvent.E3)
					.If(() => value == 0).GoTo(State.A)
				.On(SEvent.E4)
					.GoTo(State.C);

			sm.In(State.C)
				.ExecuteOnEnter(() => WriteEnter("C"))
				.ExecuteOnExit(() => WriteExit("C"))
				.On(SEvent.E1)
					.If(() => flag).Execute(() => WriteExecute("flag=true"))
					.Else().Execute(() => WriteExecute("flag=false"))
				.On(SEvent.E2)
					.If(() => value == 0).Execute(() => WriteExecute("value=0"))
					.ElseIf(() => value == 1).Execute(() => WriteExecute("value=1"))
					.ElseIf(() => value == 2).Execute(() => WriteExecute("value=2"))
					.Else().Execute(() => WriteExecute("value <> 0,1,2"));

			sm.In(State.D)
				.ExecuteOnEnter(() => WriteEnter("D"))
				.ExecuteOnExit(() => WriteExit("D"))
				.On(SEvent.E1).GoTo(State.B);

			WriteCurrentState(sm);
			WriteAction("Starting");
			sm.Start(State.A);
			WriteCurrentState(sm);

			WriteAction("Sending E1");
			sm.Handle(SEvent.E1);
			WriteCurrentState(sm);

			WriteAction("Set Flag=false and sending E1");
			flag = false;
			sm.Handle(SEvent.E1);
			WriteCurrentState(sm);

			WriteAction("Sending E1");
			sm.Handle(SEvent.E1);
			WriteCurrentState(sm);

			WriteAction("Set Flag=true and sending E1");
			flag = true;
			sm.Handle(SEvent.E1);
			WriteCurrentState(sm);

			WriteLine(ConsoleColor.DarkRed, "---PRESS A KEY---");
			Console.ReadKey();
		}

		private static IStateMachine<TState, TEvent> GetStateMachine<TState, TEvent>()
		{
			return new StateMachine<TState, TEvent>();
		}

		private static void WriteAction(string action, params object[] args)
		{
			WriteLine(ConsoleColor.Yellow, action, args);
		}

		private static void WriteCurrentState<TState, TEvent>(IStateMachine<TState, TEvent> sm)
		{
			WriteLine(ConsoleColor.Cyan, "Current State = {0}", sm.CurrentState);
		}

		private static void WriteEnter(string action, params object[] args)
		{
			WriteLine(ConsoleColor.Gray, "Entering " + action, args);
		}

		private static void WriteExit(string action, params object[] args)
		{
			WriteLine(ConsoleColor.DarkGray, "Exiting " + action, args);
		}

		private static void WriteExecute(string action, params object[] args)
		{
			WriteLine(ConsoleColor.Magenta, "Executing " + action, args);
		}

		private static void WriteLine(ConsoleColor color, string text, params object[] args)
		{
			Console.ForegroundColor = color;
			Console.WriteLine(text, args);
		}
	}
}